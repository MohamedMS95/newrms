import React from 'react';
import  {Link } from 'react-router-dom'
import axios from 'axios';

import CandidateList from './candidateList';
import Tooltip from '@material-ui/core/Tooltip';

import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';
import RestoreIcon from '@material-ui/icons/Restore';
class Candidate extends React.Component{
     txt='';
     emptycandidate={
        aadharNo:0,
        firstName:'',
        lastName:'',
        date:'',
        gender:'',
        mobile:0,
        altMobile:Number,
        email:'',
        qualification:'',
        addCertifications:'',
        totalexp:0,
        relexp:0,
        primarySkills:'',
        secondarySkills:'',
        currentCTC:0,
        expectedCTC:0,
        currentAddress:'',
        altAddress:''

  };
    
    state={
        candidate:[this.emptycandidate],  
        viewdata:[],
        searchtext:''
    }
    
    componentDidMount(){
        this.handleData();
        
    }
    handleData=()=>{

        axios.get("http://localhost:9080/candidate").then(res=>{
            console.log(res.data)
        this.setState({
            viewdata:res.data,
            candidate:res.data,
            searchtext:this.txt
        })
    })
    }
    handleSearch=()=>{
    
        let text=this.state.searchtext;
        let searchData=this.state.candidate.filter(datum=>{
             let str=datum.aadharNo.toString();
            let sub=str.substring(0,text.length);      
            console.log(str,sub)  
            return sub.includes(text);
        }
        );
        console.log(searchData);
        
        this.setState({
            ...this.state,
            viewdata:searchData
        });        
        

    } 
    handleSearchTextChange=(event: { preventDefault: () => void; target: { name: any; value: any; }; })=>{
        
        let name=event.target.name;
        let value=event.target.value;
        this.setState({
            ...this.state,
            [name]:value
        });
        

    }
    
    render(){
        const mystyle={
            color:"white",
            padding:'10px',
            backgroundColor:"white"
        };
        return (
        <div className="Button mainCandidate container-fluid "   >
            <div className="mainButton row">
            <div className="col-lg-2">
            <Button fullWidth size="large" onClick={this.handleData}
            variant="contained"
            color="primary"
            startIcon={<RestoreIcon />}
            > Refresh</Button>

                </div>
            <div className="col-lg-8">
            
        <FormControl fullWidth >
          <TextField name="searchtext" variant="outlined" color="secondary" 
          label="searchbar"placeholder="type aadhar to search"
          value={this.state.searchtext}
          onChange={this.handleSearchTextChange}
          />
          
        </FormControl>
        
            </div>
            <div className="col-lg-2">
            <Button fullWidth size="large" onClick={this.handleSearch}
            variant="contained"
            color="secondary"
            startIcon={<SearchIcon />}
            > Search</Button>

                </div>
                
            </div>
            <div className="row">
                <div className="col-lg-2"></div>
            <div className="col-lg-8"> 
            <CandidateList data={this.state.viewdata} />
            </div>
            
            </div>
        </div>);
    }
}
export default Candidate;