import React from 'react';
import TextField from '@material-ui/core/TextField';
import {makeStyles} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import axios from 'axios';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import thunk from 'redux-thunk';
import swal from 'sweetalert';
const useStyles = makeStyles(theme => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 200,
    },
  }));

class CandidateEdit extends React.Component{

    static navigationOptions = {
        //Setting the header of the screen
        title: 'EditCandidate',
      };
    candidate={
        aadharNo:0,
        firstName:'',
        lastName:'',
        date:'',
        gender:'',
        mobile:Number,
        altMobile:Number,
        email:'',
        qualification:'',
        addCertifications:'',
        totalexp:Number,
        relexp:Number,
        primarySkills:'',
        secondarySkills:'',
        currentCTC:Number,
        expectedCTC:Number,
        currentAddress:'',
        altAddress:''

    }
    state={
        cand:this.candidate
    }
    constructor(props){
        super(props);
    }
    componentDidMount(){
        //const {stat}=this.props.location.state.c;
        //console.log("state",stat)
        const aadhar=this.props.match.params.aadhar;
       
       console.log("candidateedit aadhar",aadhar);
       axios.get("http://localhost:9080/candidate/each/"+aadhar).then(res=>{
           console.log(res);
       this.setState({
           cand:res.data
       },()=>console.log(this.state.cand))});
    
    }
    handleChange=(event)=>{
        let name=event.target.name;
        let value=event.target.value;
        let newcand={...this.state.cand};
        newcand[name]=value;
        this.setState({
            cand:newcand
        }

        );

    }
    handleClear=()=>{
        this.setState({
            cand:this.candidate
        });
    }
   
    handleSubmit=(event)=>{
        let candidate=this.state.cand;
        console.log(candidate);
        event.preventDefault();
        swal({
            title: "Hey  Are you ready to Update?",
            text: "Make sure the above info is true!!!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((submit) => {
            if (submit) {
              axios.put("http://localhost:9080/candidate",candidate).then(res=>{
              
              console.log(res)
              res.status===200?swal( {
                title:"success,Form  Updation!!!!",
                text:"yeah,your form is successfully submitted",
                icon: "success",
                buttons:true
              }).then(success=>{
                if(success){
                  window.location.replace('http://localhost:3000/candidate');
                }else{
                    window.location.replace('http://localhost:3000/candidate');
                }
              })
            :swal( {
                title:"Failed,Form Submmission ,Updation!!!!",
                text:"your form is not successfully updated",
                icon: "error",
              });
               
            }).catch(res=>{
              console.log(res)
              swal({
                title:"Exception"
              ,
              icon:"error"
              });
            
            }
            );
                    } else {
              swal({
                text:"you are not completing your form updattion",
                icon:"info"
              });
            }
            
          });
        
        }
    render(){
        
        return <div className="container">
        <div className="bg-white">
            <h4 >Edit Form</h4>
            <form onSubmit={this.handleSubmit}fullWidth noValidate autoComplete="off">
      <TextField id="standard-secondary" label="AADHARNO" disabled value={this.state.cand.aadharNo} 
      onChange={this.handleChange}
      name="aadharNo" helperText="edit the aadhar" color="secondary" margin="normal" variant="filled" fullWidth/>
      <TextField
        id="filled-secondary"
        label="FIRSTNAME"
        variant="outlined"
        color="secondary"
        fullWidth
        name="firstName"
        value={this.state.cand.firstName}
        onChange={this.handleChange}
        helperText="edit the firstname"
        className={useStyles.textField}
        margin="normal"
      />
      <TextField
        id="outlined-secondary"
        label="LASTNAME"
        variant="outlined"
        color="secondary"
        margin="normal"
        name="lastName"
        value={this.state.cand.lastName}
        onChange={this.handleChange}
        fullWidth
        helperText="edit the lastname"
        
        className={useStyles.textField}
    
      />
        <RadioGroup aria-label="gender" name="gender" value={this.state.cand.gender} onChange={this.handleChange}>
          <FormControlLabel value="female" control={<Radio />} label="Female" />
          <FormControlLabel value="male" control={<Radio />} label="Male" />
          <FormControlLabel value="other" control={<Radio />} label="Other" />
          </RadioGroup>
      <TextField
        id="outlined-secondary"
        label="DATE"
        variant="outlined"
        color="secondary"
        margin="normal" 
        name="date"
        value={this.state.cand.date}
        onChange={this.handleChange}
        helperText="edit the date fromat yyyy-MM-dd"
        fullWidth
        className={useStyles.textField}
    
      />
      <TextField
        id="outlined-secondary"
        label="MOBILE"
        variant="outlined"
        color="secondary"
        margin="normal"
        name="mobile"
        value={this.state.cand.mobile}
        onChange={this.handleChange}
        helperText="edit the mobile"
        fullWidth
        className={useStyles.textField}
    
      />
        <TextField
        id="outlined-secondary"
        label="ALTERNATE MOBILE"
        variant="outlined"
        color="secondary"
        margin="normal"
        name="altMobile"
        value={this.state.cand.altMobile}
        onChange={this.handleChange}
        helperText="edit the alternate mobile"
        fullWidth
        className={useStyles.textField}
    
      />
      <TextField
        id="outlined-secondary"
        label="EMAIL"
        variant="outlined"
        color="secondary"
        margin="normal"
        name="email"
        value={this.state.cand.email}
        onChange={this.handleChange}
        helperText="edit the email"
        fullWidth
        className={useStyles.textField}
      />
      <TextField
        id="outlined-secondary"
        label="qualification"
        variant="outlined"
        color="secondary"
        margin="normal"
        name="qualification"
        value={this.state.cand.qualification}
        onChange={this.handleChange}
        helperText="edit the qualification"
        fullWidth
        className={useStyles.textField}
    
      />
      <TextField
        id="outlined-secondary"
        label="ADD CERTIFICATIONS"
        variant="outlined"
        color="secondary"
        margin="normal"
        name="addCertifications"
        value={this.state.cand.addCertifications}
        onChange={this.handleChange}
        helperText="edit certifications"
        fullWidth
        className={useStyles.textField}
    
      />
    <TextField
        id="outlined-secondary"
        label="TOTAL EXPERIENCE"
        variant="outlined"
        color="secondary"
        margin="normal"
        name="totalexp"
        value={this.state.cand.totalexp}
        onChange={this.handleChange}
        helperText="edit the exp"
        fullWidth
        className={useStyles.textField}
    
      />
    <TextField
        id="outlined-secondary"
        label="RELEVANT EXPERIENCE"
        variant="outlined"
        color="secondary"
        margin="normal"
        name="relexp"
        value={this.state.cand.relexp}
        onChange={this.handleChange}
        helperText="edit the exp"
        fullWidth
        className={useStyles.textField}
    
      />
    <TextField
        id="outlined-secondary"
        label="PRIMARY SKILLS"
        variant="outlined"
        color="secondary"
        margin="normal"
        name="primarySkills"
        value={this.state.cand.primarySkills}
        onChange={this.handleChange}
        helperText="edit the skills"
        fullWidth
        className={useStyles.textField}
    
      />
      <TextField
        id="outlined-secondary"
        label="SECONDARY SKILLS"
        variant="outlined"
        color="secondary"
        margin="normal"
        name="secondarySkills"
        value={this.state.cand.secondarySkills}
        onChange={this.handleChange}
        helperText="edit the skills"
        fullWidth
        className={useStyles.textField}
    
      />
        <TextField
        id="outlined-secondary"
        label="CURRENT CTC"
        variant="outlined"
        color="secondary"
        margin="normal"
        name="currentCTC"
        value={this.state.cand.currentCTC}
        onChange={this.handleChange}
        helperText="edit the CTC"
        fullWidth
        className={useStyles.textField}
    
      />
    <TextField
        id="outlined-secondary"
        label="EXPECTED CTC"
        variant="outlined"
        color="secondary"
        margin="normal"
        name="expectedCTC"
        value={this.state.cand.expectedCTC}
        onChange={this.handleChange}
        helperText="edit the CTC"
        fullWidth
        className={useStyles.textField}
    
      />
      <TextField
        id="outlined-secondary"
        label="CURRENT ADDRESS"
        variant="outlined"
        color="secondary"
        margin="normal"
        name="currentAddress"
        value={this.state.cand.currentAddress}
        onChange={this.handleChange}
        helperText="edit the address"
        fullWidth
        className={useStyles.textField}
    
      />
      <TextField
        id="outlined-secondary"
        label="ALTERNATE ADDRESS"
        variant="outlined"
        color="secondary"
        margin="normal"
        name="altAddress"
        value={this.state.cand.altAddress}
        onChange={this.handleChange}
        helperText="edit the address"
        fullWidth
        className={useStyles.textField}
    
      />
    <Button
        type="submit"
        variant="contained"
        color="primary"
        size="large"
        startIcon={<SaveIcon />}>
        Save
      </Button>
    
    
    


    </form>
        
            </div>
</div>;
    }
}
export default CandidateEdit;