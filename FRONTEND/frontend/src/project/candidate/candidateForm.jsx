import React from 'react';
import axios from 'axios';
import swal from 'sweetalert';
// export interface candidate{
//     aadharNo:Number,
//     firstName:String,
//     lastName:String,
//     dob:String,
//     gender:String,
//     mobile:Number,
//     altMobile:Number,
//     email:String,
//     qualification:String,
//     addCertifications:String,
//     totalexp:Number,
//     relexp:Number,
//     primarySkills:String,
//     secondarySkills:String,
//     currentCTC:Number,
//     expectedCTC:Number,
//     currentAddress:String,
//     altAddress:String
// };
class ComponentForm extends React.Component{
    emptycandidate={
        aadharNo:Number,
        firstName:'',
        lastName:'',
        date:Date,
        gender:'',
        mobile:Number,
        altMobile:Number,
        email:'',
        qualification:'',
        addCertifications:'',
        totalexp:Number,
        relexp:Number,
        primarySkills:[],
        secondarySkills:'',
        currentCTC:Number,
        expectedCTC:Number,
        currentAddress:'',
        altAddress:''

  };
  formvalidation={
    aadharNo:false,
    firstName:false,
    lastName:false,
    date:false,
    gender:false,
    mobile:false,
    
    email:false,
    qualification:false,
  
    totalexp:false,
    relexp:false,
    primarySkills:false,
  
    currentCTC:false,
    
    currentAddress:false,
    totalrelexp:false

  }
    constructor(props){
        super(props);
        this.handleChange=this.handleChange.bind(this);
        this.handleClear=this.handleClear.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
        
        }
        state={
            candidate:this.emptycandidate,
            validate:this.formvalidation
        }
    handleSubmit(event){
        event.preventDefault();
        const {aadharNo,firstName,lastName,date,email,gender,
          currentAddress,currentCTC,mobile,primarySkills,qualification,relexp,totalexp,totalrelexp}=this.state.validate;
        console.log(this.state.candidate);
        let candidate=this.state.candidate;
        if(aadharNo==true&&firstName==true&&lastName&&date==true&&email==true&&gender==true&&currentAddress==true
          &&currentCTC==true&&mobile==true&&primarySkills==true&&qualification==true&&relexp==true&&totalexp==true&&totalrelexp==true){
        swal({
            title: "Hey  Are you sure to Submit?",
            text: "Make sure the above info is true!!!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((submit) => {
            if (submit) {
              axios.post("http://localhost:9080/candidate",candidate).then(res=>{
              
              console.log(res)
              if(res.data=="Success")
              {
              swal( {
                title:"Form Submmission!!!!",
                text:"your form is successfully submitted",
                icon: "success",
                buttons:true
              }).then(success=>{
                if(success){
                  window.location.replace('http://localhost:3000/interview');
                }
              })
            }
            if(res.data[0]=="Validation Field")
            swal( {
                title:"Failed,check field..."+res.data[1],
                text:"your form is not successfully submitted",
                icon: "error",
              });
               
            }).catch(res=>{
              console.log(res)
              swal({
                title:"Exception occurs"
              ,
              icon:"error"
              });
            
            }
            );
              
              
            
          } else {
              swal({
                text:"you are not completing your form submission",
                icon:"info"
              });
            }
            
          });
        }else{
          let fieldnames=Object.getOwnPropertyNames(this.state.validate);
          console.log(fieldnames);
          fieldnames=fieldnames.filter(field=>{
            return !this.state.validate[field];
          });
          console.log(fieldnames);
          fieldnames=fieldnames.toString();
          swal({
            title:"Missing,Check the below ones",
            text:fieldnames,
            icon:"error"
          });
        }
    }
    
    handleClear(){
        console.log("clear")
        swal({
            title:"Cleaaaaaaaaaaaaaaaaaaar???!!",
            text:"Be aware all your data will be last!!!",
            icon:"info",
            buttons:true
        }).then(clear=>{
            if(clear){
                this.setState(
                    {
                        candidate:this.emptycandidate
                    }
                ); 
                swal({
                    title:"Cleaaaaaaaaaaar!!",
                    text:"oh yes,you have cleared the form data",
                    icon:"success"
                  });  
            }else{
                swal({
                    title:"Cleaaaaaaaaaaar!!",
                    text:"Aaaaaaaaaaaaaaaah,Back to the form data",
                    icon:"info"
                  });  
            }
        })
        
         
    }
    formvalidation=(name)=>{
      switch(name){
        case "aadharNo":
          if(this.state.candidate.aadharNo.toString==15){
            this.state.validate.aadharNo=true;
          }
          break;
        default:

      }
    }
    handleChange(event){
        let  name=event.target.name;
        let value=event.target.value;
        let boo;
        switch(name){
          case 'aadharNo':
            boo=value.length==12?true:false;
            break;
          case 'mobile':
              boo=value.length==12?true:false;
           
              break;
          case 'firstName':
          case 'lastName':
          case 'gender':
            
            boo=value.length!==0?true:false;
            break;
          
          case 'date':
            
            boo=value!=null?true:false;

            break;
          case 'email':
            
            //console.log(value);
            boo=(value.includes(".com")&& value!==null)?true:false;
              break;
          case 'qualification':
            boo=value!==null?true:false;
              break;
          case 'totalexp':
            boo=(value!==0)?true:false;
              break;
          case 'relexp':
            boo=(value!==0)?true:false;
              break;
          case 'primarySkills':
            boo=value!==null?true:false;
            break;
          case 'currentAddress':
            boo=value!==null?true:false;
            break;
          case 'currentCTC':
            boo=value!==0?true:false;
            break;
             
          default:
            break;
        }
        let cand={...this.state.candidate}
        cand[name]=value;
        let val={...this.state.validate}
        val[name]=boo;
        if(val.relexp==true&&val.totalexp==true){
          val.totalrelexp=true;
        }
        this.setState({
          ...this.state,
            candidate:cand,
            validate:val
        }
        );
        
    }
    
    render(){
      const startday=new Date();
      startday.setFullYear(startday.getFullYear()-60);
      const startmonth=(startday.getMonth()+1)<10?"0"+(startday.getMonth()+1).toString():startday.getMonth().toString();
   const startString=(startday.getFullYear().toString())+"-"+startmonth+"-"+(startday.getDate().toString());
   const endday=new Date();
   endday.setFullYear(endday.getFullYear()-18);
   const endmonth=(endday.getMonth()+1)<10?"0"+(endday.getMonth()+1).toString():endday.getMonth().toString();
const endString=(endday.getFullYear().toString())+"-"+endmonth+"-"+(endday.getDate().toString());
   
        const mystyle={
            color:"black",
            backgroundColor:"white"
         
        };
        const submitstyle={
            color:"white",
            backgroundColor:"black"
        };
        return <div className="form " style={mystyle} className="col-lg-4 offset-lg-3">
            <h1>Candidate Form</h1>
            <form className="needs-validation" onSubmit={this.handleSubmit} >
            <label >AADHAR NUMBER</label><sup className="mandatory">*</sup>
            <input id="aadharno" name="aadharNo" placeholder="Enter your aadhar no." 
             onChange={this.handleChange} value={this.state.candidate.aadharNo} size={50}
             className="form-control"
             pattern="[0-9]{12}"
            title="fill the aadhar as per the instruction"
             aria-describedby="aadharNohelp"
             required/>
            <small id="aadharNohelp" className="form-text text-muted">Aadhar Number must be  12 pure numbers only</small>
            
            
            <br/>
            <label>FIRSTNAME</label><sup className="mandatory">*</sup>
            <input id="firstname" 
            name="firstName" 
            pattern="[A-Za-z]*|[A-Za-z]*\s[A-Za-z]*"
            placeholder="Enter your firstname" 
            title="fill the first name as per the instruction"
            onChange={this.handleChange} 
            value={this.state.candidate.firstName} 
            className="form-control"
            aria-describedby="firstNamehelp"
            required/>
            <small id="firstNamehelp" className="form-text text-muted">Alphabets only,case insensitive if you have middle name make space and type it format:<strong>firstname middlename</strong></small>
            
            
            <br/>
            
            <label>LASTNAME</label><sup className="mandatory">*</sup>
            <input id="lastname"
             name="lastName"placeholder="Enter your lastname"
              onChange={this.handleChange} value={this.state.candidate.lastName} 
              className="form-control"
              aria-describedby="lastNamehelp"
              pattern="[A-Za-z]*|[A-Za-z]*\s[A-Za-z]*"
              
              required/>
              <small id="lastNamehelp" className="form-text text-muted">Alphabets only,case insensitive  format:<strong>lastname </strong></small>
              
            <br/>
            
            <label>DATE OF BIRTH</label><sup className="mandatory">*</sup>
            <input id="dob" placeholder="yyyy-MM-dd" 
            type="date"
            min={startString}
            max={endString}
            name="date" onChange={this.handleChange} 
            className="form-control"
            aria-describedby="datehelp"
            
            value={this.state.candidate.date}size={50}
            required/>
            <small id="datehelp" className="form-text text-muted">Age should be  <strong>>=18 </strong></small>
              
            <br/>
            <div className="form-checked" required>
            <label>GENDER</label><sup className="mandatory">*</sup>
            
            <label><input id="gender" name="gender" type="radio" value="male" onClick={this.handleChange} size={50} />Male</label>&nbsp;&nbsp;&nbsp;
            <label><input id="gender" name="gender" type="radio" value="female" onClick={this.handleChange} size={50}/>Female</label>&nbsp;&nbsp;&nbsp;
            <label><input id="gender" name="gender" type="radio" value="others" onClick={this.handleChange} size={50}/>Others</label>
            </div>
            
            <br/>
            <label>MOBILE</label><sup className="mandatory">*</sup>
            <input id="mobile" placeholder="Enter the mobile number" name="mobile" 
            onChange={this.handleChange} value={this.state.candidate.mobile} 
            className="form-control"
            size={50}
            aria-describedby="mobilehelp"
            pattern="[0-9]{12}"
            required/>
            <small id="mobilehelp" className="form-text text-muted">* this will be used for communication please be aware Format:CountryCode and 10 digits eg: <u><strong>91</strong>9787252728</u></small>
            
            <br/>
            <label>ALTERNATE MOBILE</label>
            <input id="altmobile" placeholder="Enter the alternate mobile no." 
            name="altMobile" onChange={this.handleChange} value={this.state.candidate.altMobile} 
            className="form-control"
            pattern="[0-9]{12}"
            aria-describedby="altMobilehelp"
            size={50}/>
            <small id="altMobilehelp" className="form-text text-muted">Format:CountryCode and 10 digits eg: <u><strong>91</strong>9787252728</u></small>
            
            <br/>
            <label>EMAIL</label><sup className="mandatory">*</sup>
            <input id="email" type="email" placeholder="Enter your mail" name="email"
             onChange={this.handleChange} value={this.state.candidate.email} 
             className="form-control"
             size={50}
             pattern="[A-Za-z0-9]*@[A-Za-z]*.com"
             required/>
             <small id="emailhelp" className="form-text text-muted">* this will be used for communication please be aware Format eg: <strong>XXXXXX@gmail.com</strong></small>
             
            <br/>
            <label>QUALIFICATION</label><sup className="mandatory">*</sup>
            <input id="qualification" placeholder="your qualification" name="qualification" 
            onChange={this.handleChange} value={this.state.candidate.qualification}
            className="form-control"
            size={50}
            aria-describedby="qualificationhelp"
            pattern="[A-Z].[A-Za-z\\s]*|[A-Za-z]"
            required/>
            <small id="qualificationhelp" className="form-text text-muted">Format eg: <strong>B.E,B.Tech</strong></small>
            
            <br/>
            <label>CERTIFICATIONS</label>
            <input id="additionalcertifications" placeholder="enter the certifications" 
            name="addCertifications"onChange={this.handleChange} 
            className="form-control"
            //pattern="[a-zA-Z]"
            value={this.state.candidate.addCertifications} 
            size={50}/>
            
            <br/>
            <label>TOTAL EXPERIENCE</label><sup className="mandatory">*</sup>
            <input id="totalexperience" placeholder="enter the exp " name="totalexp" 
            onChange={this.handleChange} value={this.state.candidate.totalexp}
            className="form-control"
            pattern="[0-9]|[0-9].[0-9]"
            size={50}
            required/>
            <br/>
            <label>RELEVANT EXPERIENCE</label><sup className="mandatory">*</sup>
            <input id="relevantexperience" placeholder="enter the exp "name="relexp" 
            onChange={this.handleChange} value={this.state.candidate.relexp}
            className="form-control"
            aria-describedby="relexphelp"
            max={this.state.candidate.totalexp}
            min={0}
            pattern="[0-9]|[0-9].[0-9]"
            size={50}
            required/>
            <small id="relexphelp" className="form-text text-muted">in years</small>
            
            <br/>
            <label>PRIMARY SKILLS</label><sup className="mandatory">*</sup>
            <input  id="primaryskills"  placeholder="pri skills" name="primarySkills"
             onChange={this.handleChange} value={this.state.candidate.primarySkills} 
             className="form-control"
             aria-describedby="primarySkillshelp"
             size={50}
             required/>
             <small id="primarySkillshelp" className="form-text text-muted">enter the skills </small>
             
             <br/>
            <label>SECONDARY SKILLS</label>
            <input  id="secondaryskills"  placeholder="sec skills" 
            name="secondarySkills" onChange={this.handleChange} 
            value={this.state.candidate.secondarySkills} 
            className="form-control"
            size={50}/>
            <small id="secondarySkillshelp" className="form-text text-muted">enter the additional skills</small>
            
            <br/>
            <label className="ctc">CURRENT CTC</label><sup className="mandatory">*</sup>
            <input  id="currentctc"  placeholder="ctc in numbers" name="currentCTC"
             onChange={this.handleChange} value={this.state.candidate.currentCTC}
             className="form-control"
             size={50}
             pattern="[0-9]*"
             required/>
             <small id="currentCTChelp" className="form-text text-muted">enter the CTC in numbers</small>
             
            <br/>
            <label>EXPECTED CTC</label>
            <input  id="expectedctc"  placeholder="ctc in numbers" name="expectedCTC" 
            onChange={this.handleChange} value={this.state.candidate.expectedCTC} 
            className="form-control"
            pattern="[0-9]*"
             
            size={50}
            />
            <small id="expectedCTChelp" className="form-text text-muted">enter the CTC</small>
            
            <br/>
            <label>CURRENT ADDRESS</label><sup className="mandatory">*</sup>
            <input  id="currentaddress"  name="currentAddress"onChange={this.handleChange}
             value={this.state.candidate.currentAddress} size={50}
             className="form-control"
              
             required/>
             <small id="currentAddresshelp" className="form-text text-muted">enter the current address</small>
             
             <br/>
            <label>ALTERNATE ADDRESS</label>
            <input  id="altaddress"  name="altAddress" onChange={this.handleChange} 
            value={this.state.candidate.altAddress} size={50}
            className="form-control"
            />
            <small id="altAddresshelp" className="form-text text-muted">enter the alternate address</small>
            
            <br/>
            <input className=" btn btn-danger mr-3"type="reset" value="clear" 
            onClick={this.handleClear}
            />
            
            <input className="btn btn-success mr-1" type="submit" value="Submit"   size={30}/>
            </form>
            <br/>
            </div>;
    }
}
export default ComponentForm;