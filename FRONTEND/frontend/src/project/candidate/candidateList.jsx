import React from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';

import MatButton from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import {deepOrange} from '@material-ui/core/colors';
import IconButtons from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import axios  from 'axios';
import {purple} from '@material-ui/core/colors';

import CandidateDetails from './candidateDetails.jsx';
import { Tooltip, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import swal from 'sweetalert';
const my=purple[500];
const style = makeStyles(theme=>({
    root: {
      maxWidth: 700,
      display:'flex',
      margin:theme.spacing(1),
      color: theme.palette.getContrastText(deepOrange[500]),
        
      backgroundColor:deepOrange[500]
    },
    media: {
      height: 300,
    },
    avatar:{
      
        color: theme.palette.getContrastText(deepOrange[500]),
        backgroundColor: deepOrange[500]
    },
    small:{
      width:theme.spacing(3),
      height:theme.spacing(3)
    },
    large:{
      width:theme.spacing(7),
      height:theme.spacing(7)
    
    },
    
  }));
class CandidateList extends React.Component{
    
    constructor(props){
        super(props);
        
    }
     func=this.props.met;
    handleDelete(event){
      event.preventDefault();
      let aadhar=event.target.value;
    
      console.log(aadhar);
      swal({
        title:"Finally,Deleeeeeeeeeete!!!!???",
        text:"you have gone mad ",
        icon:"warning",
        buttons:true
    }).then(del => {
        if(del){
            axios.delete("http://localhost:9080/candidate/"+aadhar).then(res=>{
            console.log("delete candiate",res);
            if(res.status===200){
                swal({
                    title:"Success,I told you to delete this man",
                    text:"yeah,my weight got reduced",
                    icon:"success",
                    buttons:true
                }).then(res=>{
                    window.location.reload();
                })
                
            }
        }).catch(res=>swal("Exception occurs wow "));
        }else{
            swal({
                title:"No Deletion,Atleast  You heard me",
                text:"no delete,I backed it",
                icon:"info",
                
            })
        }
    })
    

      
    }
    // EditCandidate(event)
    // {
    //   const { navigate } = this.props.navigation;
    //   event.preventDefault();
    //   let datum=event.target.value;
    //   navigate('EditCandidate',datum);
    // }
    handleCard=(data)=>{
      let keys=data.keys;
      let source='',s='',s2='';
        let mydata=data.map(data=>
            {
              if(data.gender=="male"){
                source="https://maxcdn.icons8.com/Share/icon/Users/administrator_male1600.png"
                }else{
                source="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn1.iconfinder.com%2Fdata%2Ficons%2Favatars-1-5%2F136%2F60-512.png&f=1&nofb=1"
                }
                s='/candidate/edit/'+data.aadharNo;
                s2='/interview/addInterview/'+data.aadharNo;
               return  (
                 <div key={data.aadharNo+data.lastName}className="bg-success">
               <Card  >
      <CardActionArea color='info'>
        <div className={style.avatar}>
          
          
        <Avatar alt={data.firstName} src={source} className={style.large} />
        </div>
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {data.firstName+" "+data.lastName}
          </Typography>
          <Typography  variant="body2" color="textSecondary" component="p">
            {data.aadharNo}
          </Typography>
        </CardContent>
        <CandidateDetails  candidatedata={data}/>
      </CardActionArea>
      <CardActions>
        <Tooltip color="secondary" title="Click to Delete">
        
          <button  type="button"className="btn btn-outline-danger" value={data.aadharNo} onClick={this.handleDelete.bind(this)}>
          DELETE
          </button>
          
          </Tooltip>
          
         <Link to={{pathname:s,state:{c:data}}}>
        <Button  size="large" color="primary" onClick={this.EditCandidate} value={data} >
          Edit
        </Button>
        </Link>
        <Link to={{pathname:s2,state:{c:data}}}>
        <Button  size="large" color="secondary" onClick={this.EditCandidate} value={data} >
          Schedule Interview
        </Button>
        </Link>
      </CardActions>
    </Card>
      
        <br/>  
        </div>       ) }
        );
        return mydata;
    }
    render(){
        return <div >
          <br/>
          
        
                
              
                  
              
            <div key="data">
            {this.handleCard(this.props.data)}
            </div>
            </div>;
    }
}
export default CandidateList;