import React from 'react';
import axios from 'axios';
class CandidateFile extends React.Component {
    state = {
        file: ''
    }
    handleSubmit = (event) => {
        event.preventDefault();
        const formData = new FormData();
        formData.append('file', this.state.file)
        axios.post("http://localhost:9080/upload", formData).then(res => {
            console.log(res);
        }).catch(res => {
            console.log(res)
        })

    }
    handleChange = (event) => {
        let my = event.target.files[0];
        event.preventDefault();
        this.setState({
            file: my
        });

    }
    render() {

        return <div >
            <form onSubmit={this.handleSubmit}>
                <input type="file" onChange={this.handleChange}  />
                <input type="submit" />
            </form>
        </div>;
    }
}
export default CandidateFile;