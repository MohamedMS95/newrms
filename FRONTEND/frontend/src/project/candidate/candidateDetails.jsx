import React from 'react';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const Cell = withStyles(theme => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);
  
  const Row = withStyles(theme => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
      },
    },
  }))(TableRow);
  
  
  
  
  const tableStyles = makeStyles({
    table: {
      minWidth: 400,
    },
  });
  
const useStyles = makeStyles(theme => ({
    root: {
      width: '100%',
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: '33.33%',
      flexShrink: 0,
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
    },
  }));
const CandidateDetails=(props)=>{
    let data=props.candidatedata;
    let keys=Object.getOwnPropertyNames(data);
    
    
    const classes = tableStyles();
  const [expanded, setExpanded] = React.useState(false);
    const handleChange = panel => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
      };
    return (
        <ExpansionPanel  expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
          backgroundColor="primary"
        >
        <Typography  variant="h5" component="h6" color="primary"> VIEW </Typography>
          
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Typography>
          <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <Cell>Field</Cell>
            <Cell >Value</Cell>
            
          </TableRow>
        </TableHead>
        <TableBody>
          {keys.map(key => (
            <Row key={key}>
              <Cell component="th" scope="row">
                {key}
              </Cell>
              <Cell align="left">{data[key]}</Cell>
              
            </Row>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
          </Typography>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
    
}
export default CandidateDetails;