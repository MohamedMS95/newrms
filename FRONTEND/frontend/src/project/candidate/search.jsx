import React from 'react';
import tags from 'react-tagsinput';

class Search extends React.Component{
    render(){

        return <div className="container">
                <h3 className="d-block bg-light text-dark">Search Page</h3>
                <form className="form-group">
                    <div className="form-row">
                    <input className="col-lg-3 form-control border-dark" type="search" name="skills" placeholder="skills"/>
                    <input className="col-lg-3 form-control border-dark" type="search" name="location" placeholder="location"/>
                    <input className="col-lg-3 form-control border-primary"  type="search" name="fromExp" placeholder="min experience"/>
                    <input className="col-lg-3 form-control border-primary" type="search" name="toExp" placeholder="max experience"/>
                    </div>
                    <div className="form-row">

                    <input className="btn btn-outline-danger" type="submit"/>
                    </div>
                    </form>
            </div>;
    }
}
export default Search;