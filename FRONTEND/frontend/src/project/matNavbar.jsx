import React from 'react';
import HomeIcon from '@material-ui/icons/Home';
import PersonAdd from '@material-ui/icons/PersonAdd';
import Person from '@material-ui/icons/Person';
import People from '@material-ui/icons/People';
import GroupAdd from '@material-ui/icons/GroupAdd';
import {Link} from 'react-router-dom';
import ViewComfy from '@material-ui/icons/ViewComfy';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AppBar from '@material-ui/core/AppBar';
import ToolBar from '@material-ui/core/Toolbar'
import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';
import SearchIcon from '@material-ui/icons/Search';
const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
});



export default function TemporaryDrawer() {
  
  const classes = useStyles();
  const [state, setState] = React.useState({
    
    left: false,
    
  });
  
    

   const handleBarName=(name)=>{
    //event.preventDefault();
    //nameBar=event.target.value;
   // nameBar=name;
    //console.log(nameBar);
}

  const toggleDrawer = (side, open) => event => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [side]: open });
  };

  const sideList = side => (
    <div
      className={classes.list}
      role="presentation"
      onClick={toggleDrawer(side, false)}
      onKeyDown={toggleDrawer(side, false)}
    >
      <List>
      <Link to='/home'  onClick={()=>handleBarName("HOM")}>
      <ListItem button key="Home"  >
          
            <ListItemIcon><HomeIcon/></ListItemIcon>
            <ListItemText primary="Home" variant="filled" />
                     </ListItem>
                     </Link>

          
          <Divider />
          <Link to='/candidate/search' >
      <ListItem button key="Search">
          
            <ListItemIcon><SearchIcon/></ListItemIcon>
            <ListItemText primary="search" variant="filled" />
                     </ListItem>
                     </Link>

          
          <Divider />
          <Link to='/candidate' onClick={()=>handleBarName("CANDIDATE")}>
          <ListItem button key="Candidate">
            <ListItemIcon><Person/></ListItemIcon>
            <ListItemText primary="Candidate" variant="outlined" />
          </ListItem>
          </Link>
          <Link to='/candidate/candidateForm'>
          <ListItem button key="candidateadd">
            <ListItemIcon><PersonAdd/></ListItemIcon>
            <ListItemText primary="Add Candidate" variant="filled" />
          </ListItem>
          </Link>
          <Divider />
          <Link to='/interview'>
          <ListItem button key="Interview">
            <ListItemIcon><People/></ListItemIcon>
            <ListItemText primary="Interview" variant="filled" />
          </ListItem>
          </Link>
          <Link to='/interview/addInterview'>
          <ListItem button key="interviewadd">
            <ListItemIcon><GroupAdd/></ListItemIcon>
            <ListItemText primary="Add Interview" variant="filled" />
          </ListItem>
          </Link>
      
      </List>
      <Divider />
      
    </div>
  );

  

  return (
    <div >
      <AppBar  position="fixed">
        <ToolBar variant="dense">
      <Button onClick={toggleDrawer('left', true)} size="large" 
      
    
      >
        
        <MenuIcon fontSize="large" color="error" />
       
        </Button>
        <Typography variant="h6">
        MENU
      </Typography>
      </ToolBar>
      </AppBar>
      <Drawer open={state.left} onClose={toggleDrawer('left', false)}>
        {sideList('left')}
      </Drawer>
      <br/>
      <br/>
      <br/>
    </div>
  );
}
