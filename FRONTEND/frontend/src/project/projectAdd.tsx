import React from 'react';
import { Link } from 'react-router-dom';

class ProjectAdd extends React.Component<any, any> {
  render() {
    return (
      <div className="row">
        <div className="col-lg-2 m-3">
          <Link to="/project/add">
            <button className="btn btn-danger ">ADD PROJECT</button>
          </Link>
        </div>
      </div>
    );
  }
}

export default ProjectAdd;
