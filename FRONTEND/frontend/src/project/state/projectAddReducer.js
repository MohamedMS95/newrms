import { SET_FORM_DATA } from './projectAddActions';

const initialState = {
  formData: {
    name: '',
    description: '',
    owner: '',
    type: 'scrum'
  }
};

function projectAddReducer(state = initialState, action) {
  switch (action.type) {
    case SET_FORM_DATA: {
      return {
        ...state,
        formData: {
          ...state.formData,
          [action.name]: [action.value]
        }
      };
    }
    default:
      return state;
  }
}

export default projectAddReducer;
