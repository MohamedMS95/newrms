import React from 'react';
import { connect } from 'react-redux';
import {
  searchProjectAction,
  changeSearchTerm as changeSearchTermAction
} from './state/actions';

class ProjectFilter extends React.Component<any> {
  constructor(props: any) {
    super(props);
    this.onInputChange = this.onInputChange.bind(this);
  }
  onInputChange(e: any) {
    this.props.changeSearchTerm(e.target.value);
  }
  render() {
    return (
      <input
        value={this.props.searchTerm}
        onChange={this.onInputChange}
        className="form-control mt-2 p-3"
        placeholder="enter project name"
      />
    );
  }
}

function mapStateToProps(state: any) {
  return {
    searchTerm: state.projectState.searchTerm
  };
}
function mapDispatchToProps(dispatch: any) {
  return {
    searchProject: (searchTerm: string) =>
      dispatch(searchProjectAction(searchTerm)),
    changeSearchTerm: (term: string) => dispatch(changeSearchTermAction(term))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectFilter);
