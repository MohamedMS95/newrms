import React from 'react';
import TextField from '@material-ui/core/TextField';
import {makeStyles} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import axios from 'axios';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Item from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import swal from 'sweetalert';
const useStyles = makeStyles(theme => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 200,
    },
  }));

class InterviewEdit extends React.Component{

    candidate={
        aadharNo:0,
         firstName:'',
         lastName:'',
         date:'',
         gender:'',
         mobile:Number,
         altMobile:Number,
         email:'',
       qualification:'',
         addCertifications:'',
         totalexp:Number,
         relexp:Number,
         primarySkills:'',
         secondarySkills:'',
         currentCTC:Number,
         expectedCTC:Number,
         currentAddress:'',
         altAddress:''
    
    }
    interview={
      id:Number,
      candidate:this.candidate,
      mode:'',
      date:'',
      location:'',
      reccommendation:'',
      panel:'',
      ref:''
    }
    state={
      interview:this.interview,
      
    }
    handleChange=(event)=>{
        let name=event.target.name;
        let value=event.target.value;
        let inter={...this.state.interview}
        inter[name]=value;
        this.setState({
            interview:inter
        });
    }
    handleSubmit=(event)=>{
        event.preventDefault();
        let interview=this.state.interview;
        swal({
            title: "Hey  Are you ready to Save?",
            text: "Make sure the above info is true!!!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((submit) => {
            if (submit) {
              axios.put("http://localhost:9080/interview",interview).then(res=>{
              
              console.log(res)
              res.status===200?swal( {
                title:"success,Form  Updation!!!!",
                text:"yeah,your form is successfully submitted",
                icon: "success",
                buttons:true
              }).then(success=>{
                if(success){
                  window.location.replace('http://localhost:3000/interview');
                }else{
                    window.location.replace('http://localhost:3000/interview');
                }
              })
            :swal( {
                title:"Failed,Form Submmission ,Updation!!!!",
                text:"your form is not successfully updated",
                icon: "error",
              });
               
            }).catch(res=>{
              console.log(res)
              swal({
                title:"Exception"
              ,
              icon:"error"
              });
            
            }
            );
              
              
            
                    } else {
              swal({
                text:"you are not completing your form updattion",
                icon:"info"
              });
            }
            
          });
          
          }
    handleClear=(event)=>{
        event.preventDefault();
        this.setState({
            interview:this.interview
        });
    }
    componentDidMount(){
        let aadhar=this.props.match.params.aadhar;
        console.log(aadhar)
        axios.get("http://localhost:9080/interview/"+aadhar).then(res=>{
            console.log(res.data);
            this.setState({
                interview:res.data
            })
        }).catch(res=>console.log(res))
    }
    render(){

        return <div className="container">
        <div className="bg-light">
            <h4 >Edit Form</h4>
            <form onSubmit={this.handleSubmit}  noValidate autoComplete="off" fullWidth>
          
      <TextField
        id="filled-secondary"
        
        variant="outlined"
        color="secondary"
        
        name="candidate"
        
        value={this.state.interview.candidate.aadharNo}
        onChange={this.handleChange}
        helperText="can't edit the candidate"
        className={useStyles.textField}
        margin="normal"
        fullWidth
        disabled
      />
    <InputLabel id="modeofinterview" >Mode Of Interview</InputLabel>
<Select

          labelId="demo-simple-select-error-label"
          id="modeofinterview"
          value={this.state.interview.mode}
          onChange={this.handleChange}
          fullWidth
          variant="filled"
          margin="normal"
            name="mode"
        >
          <MenuItem value="null" disabled>
            <em>Mode Of Interview</em>
          </MenuItem>
          <MenuItem value="technical">Technical</MenuItem>
          <MenuItem value="online video conference">Online Video Conference</MenuItem>
          <MenuItem value="personal">Personal</MenuItem>
        </Select>
      <TextField
        id="filled-secondary"
        label="Date"
        variant="outlined"
        color="secondary"
        fullWidth
        name="date"
        value={this.state.interview.date}
        onChange={this.handleChange}
        helperText="edit the date"
        className={useStyles.textField}
        margin="normal"
      />
      <TextField
        id="outlined-secondary"
        label="Location"
        variant="outlined"
        color="secondary"
        margin="normal"
        name="location"
        value={this.state.interview.location}
        onChange={this.handleChange}
        fullWidth
        helperText="edit the location"
        
        className={useStyles.textField}
    
      />
        
        <TextField
        id="outlined-secondary"
        label="Recommendation"
        variant="outlined"
        color="secondary"
        margin="normal"
        name="reccommendation"
        value={this.state.interview.reccommendation}
        onChange={this.handleChange}
        helperText="edit the recommendation"
        fullWidth
        className={useStyles.textField}
    
      />
      <InputLabel id="panel" >Panel</InputLabel>
      <Select
            
          labelId="demo-simple-select-error-label"
          id="panel"
          value={this.state.interview.panel}
          variant="filled"
          onChange={this.handleChange}
          fullWidth
          displayEmpty
          helperText="choose it"
          margin="normal"
            name="panel"
        >
          <MenuItem value="null" disabled>
            <em>Panel</em>
          </MenuItem>
          <MenuItem value="A">A</MenuItem>
          <MenuItem value="B">B</MenuItem>
          <MenuItem value="C">C</MenuItem>
        </Select>

      <TextField
        id="outlined-secondary"
        label="References"
        variant="outlined"
        color="secondary"
        margin="normal"
        name="ref"
        value={this.state.interview.ref}
        onChange={this.handleChange}
        helperText="edit the ref"
        fullWidth
        className={useStyles.textField}
    
      />
    <Button
        type="submit"
        variant="contained"
        color="secondary"
        size="large"
        startIcon={<SaveIcon />}>
        Save
      </Button>
    
    
    


    </form>
        
            </div>
</div>

            ;
    }
}
export default InterviewEdit;
