import React from 'react';
import { Link } from 'react-router-dom';
import InterviewList from './interviewList';
import axios from 'axios';
import sweet from 'sweetalert';
import InterviewCollapse from './interviewCollapse';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import RestoreIcon from '@material-ui/icons/Restore';
import SearchIcon from '@material-ui/icons/Search';
class Interview extends React.Component{
    candidate={
        aadharNo:'',
        firstName:'',
        lastName:'',
        date:'',
        gender:'',
        mobile:Number,
        altMobile:Number,
        email:'',
        qualification:'',
        addCertifications:'',
        totalexp:Number,
        relexp:Number,
        primarySkills:'',
        secondarySkills:'',
        currentCTC:Number,
        expectedCTC:Number,
        currentAddress:'',
        altAddress:''

    }
    interview={
        id:Number,
        candidate:this.candidate,
        mode:'',
        date:'',
        location:'',
        reccommendation:'',
        panel:'',
        ref:''
      }
      txt='';
      state={
          inter:[],
          searchtext:this.txt,
        viewdata:[]
      }
      handleData=()=>{
        axios.get("http://localhost:9080/interview").then(res=>{
              console.log(res);
              this.setState({
                  viewdata:res.data,
                  inter:res.data,
                  searchtext:this.txt
              });
            }).catch(res=>console.log(res))

            }
    handleSearch=(event)=>{
        event.preventDefault();
        let text=this.state.searchtext;
        let searchData=this.state.inter.filter(datum=>{
            let str=datum.candidate.aadharNo.toString();
            let sub=str.substring(0,text.length);
            return sub.includes(text);
        }
        );
        this.setState({
            ...this.state,
            viewdata:searchData
        });        
        

    } 
    handleSearchTextChange=(event)=>{
        event.preventDefault();
        let name=event.target.name;
        let value=event.target.value;
        this.setState({
            ...this.state,
            [name]:value
        });


    }
    
      componentDidMount(){
          this.handleData();
                }
    render(){
        return <div className="container-fluid ">
            <br/>
            <div className="row">
            <div className="col-lg-2">
            <Button fullWidth size="large" onClick={this.handleData} 
            startIcon={<RestoreIcon/>}
            color="primary"
            variant="contained"
            >Refresh</Button>

                </div>
            <div className="col-lg-8">
            
        <FormControl fullWidth >
          <TextField name="searchtext" variant="outlined" color="secondary" 
          label="searchbar"placeholder="type aadharNo to filter "
          value={this.state.searchtext}
          onChange={this.handleSearchTextChange}
          />
          
        </FormControl>
        
            </div>
            <div className="col-lg-2">
            <Button fullWidth size="large" onClick={this.handleSearch} 
            startIcon={<SearchIcon />}
            color="secondary"
            variant="contained"
            >Search </Button>

                </div>
            
                </div>
            <div className="row bg-light">
                <div className="col-lg-10">
                <h1><i>List Of Interviews</i></h1>
                <InterviewList data={this.state.viewdata}/>
                </div>
                
                </div>
            </div>;
    }
}
export default Interview;