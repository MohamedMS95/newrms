import React from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';
class InterviewForm extends React.Component{

  
  candidate={
    aadharNo:Number,
     firstName:'',
     lastName:'',
     date:'',
     gender:'',
     mobile:Number,
     altMobile:Number,
     email:'',
   qualification:'',
     addCertifications:'',
     totalexp:Number,
     relexp:Number,
     primarySkills:'',
     secondarySkills:'',
     currentCTC:Number,
     expectedCTC:Number,
     currentAddress:'',
     altAddress:''

}
  

interview={
  id:Number,
  candidate:this.candidate,
  mode:'',
  date:Date,
  location:'',
  reccommendation:'',
  panel:'',
  ref:'',
  mobile:'',
  email:'',
  

}
validateForm={
  candidate:false,
  mode:false,
  date:false,
  location:false
  
}
state={
  interview:this.interview,
  candidateaadhar:[],
  
}
    constructor(props){
      super(props);
      this.handleChange=this.handleChange.bind(this);
      this.handleClear=this.handleClear.bind(this);
      this.handleSubmit=this.handleSubmit.bind(this);
    }
    
    handleSubmit(event){
      event.preventDefault();
        let interview=this.state.interview;
        console.log("submit",interview);
      
    
      swal({
        title: "Hey  Are you sure to Submit?",
        text: "Make sure the above info is true!!!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((submit) => {
        if (submit) {
          axios.post("http://localhost:9080/interview",interview).then(res=>{
          
          console.log("res===="+res)
          res.status===200?swal( {
            title:"Form Submmission!!!!",
            text:"your form is successfully submitted",
            icon: "success",
            buttons:true
          }).then(success=>{
            if(success){
              window.location.replace('http://localhost:3000/interview');
            }
          })
        :swal( {
            title:"Failed,Form Submmission!!!!",
            text:"your form is not successfully submitted",
            icon: "error",
          });
           
        }).catch(res=>{
          console.log("response "+res)
          swal({
            title:"Exception"
          ,
          icon:"error"+res.status
          });
        
        }
        );
          
          
        
                } else {
          swal({
            text:"you are not completing your form submission",
            icon:"info"
          });
        }
        
      });
        
      
    }
    handleClear=(event)=>{
      event.preventDefault();
      swal({
        title:"Cleaaaaaaaaaaaaaaaaaaar???!!",
        text:"Be aware all your data will be last!!!",
        icon:"info",
        buttons:true
    }).then(clear=>{
      if(clear){
        this.setState({
          interview:this.interview
        });
        swal({
          title:"Cleaaaaaaaaaaar!!",
          text:"oh yes,you have cleared the form data",
          icon:"success"
        });
      }else{
        swal({
          title:"Cleaaaaaaaaaaar!!",
          text:"Aaaaaaaaaaaaaaaah,Back to the form data",
          icon:"info"
        });
      }
    })
      
    } 
    handleChange=(event)=>{
      let name=event.target.name;
      let value=event.target.value;
      let inter={...this.state.interview};
      
      if(name=="candidate"){
          inter.candidate.aadharNo=value;
          
      }
      else{
        console.log('others')
        inter[name]=value;
        
      }
        
      
        
      
      
      this.setState({
          ...this.state,
          interview:inter,

      });
      console.log(this.state.interview.date)
      
    }
    componentDidMount(){
      let aad=this.props.match.params.aadhar;
      let intero=this.state.interview;
      intero.candidate.aadharNo=aad;
      this.setState({
        ...this.state,
        inter:intero
      });
    }
    
    renderLocationOrMobileOrEmail=()=>{
      switch(this.state.interview.mode){
        case "personal":
       return <div className="form-group">
          <label htmlFor="location">Location<sup className="mandatory">*</sup></label>
          <input
            type="text"
            className="form-control border border-success"
            name="location"
            placeholder="Enter your location"
            onChange={this.handleChange}
            value={this.state.interview.location}
            required
          />
        </div>;
        break;
        case "telephonic":
          return <div className="form-group">
          <label htmlFor="mobile">Telephone<sup className="mandatory">*</sup></label>
          <input
            type="text"
            className="form-control border border-success"
            name="mobile"
            placeholder="Enter the mobile number"
            onChange={this.handleChange}
            value={this.state.interview.mobile}
            required
            pattern="[0-9]{12}"
          />
        </div>;
        break;
        case "online video conference":
        return <div className="form-group">
          <label htmlFor="email">Email<sup className="mandatory">*</sup></label>
          <input
            type="email"
            className="form-control border border-success"
            name="email"
            placeholder="Enter email address"
            onChange={this.handleChange}
            value={this.state.interview.email}
            required
            pattern="[A-Za-z0-9]*@[A-Za-z]*.com"
          />
        </div>;
        break;
        default:
          return <div className="form-group">
          <label htmlFor="mobile">Telephone<sup className="mandatory">*</sup></label>
          <input
            type="text"
            className="form-control border border-success"
            name="mobile"
            placeholder="Enter the mobile number"
            onChange={this.handleChange}
            value={this.state.interview.mobile}
            required
            pattern="[0-9]{12}"
          />
        </div>;
        break;
          
      }
    
    }
    render(){
      const today=new Date();
      const month=(today.getMonth()+1)<10?"0"+(today.getMonth()+1).toString():today.getMonth().toString();
   const todayString=(today.getFullYear().toString())+"-"+month+"-"+(today.getDate().toString());
      let limit=new Date();
      limit.setFullYear(new Date().getFullYear()+1);
      const endmonth=(limit.getMonth()+1)<10?"0"+(limit.getMonth()+1).toString():limit.getMonth().toString();
      const endString=(limit.getFullYear().toString())+"-"+endmonth+"-"+(limit.getDate().toString());
      //console.log(endString);
      let ar=this.state.candidateaadhar;
      let newar=ar.map(datum=>{
      return(<option key={datum.aadharNo}value={datum.aadharNo}>{datum.aadharNo+" : "+datum.firstName+" "+datum.lastName}</option>);
      });
        return <div className="col-lg-5 offset-lg-3">
            <h1>InterviewForm</h1>
            <form className="border border-dark p-3" onSubmit={this.handleSubmit}>
            <div className="form-group">
          <label htmlFor="type">Candidate<sup className="mandatory">*</sup></label>
          <input
            type="text"
            className="form-control"
            name="candidate"
            placeholder="Enter candidate aadhar"
            onChange={this.handleChange}
            
            pattern="[0-9]{12}"
            value={this.state.interview.candidate.aadharNo}
            required={false}
            disabled={true}
          />
          
          <br/>
          
        </div>
        <div className="form-group">
          <label htmlFor="mode">Mode Of Interview<sup className="mandatory">*</sup></label>
          <select
            className="form-control border border-danger"
            name="mode" onClick={this.handleChange}
            required
            >
            <option value="null" disabled>--Select the mode--</option>
            
            <option value="telephonic">Telephonic</option>
            <option value="online video conference" >Online Video Conference</option>
            <option value="personal">Personal</option>
          </select>
          
        </div>
              {this.renderLocationOrMobileOrEmail()}
        <div className="form-group ">
          <label htmlFor="date">Date<sup className="mandatory">*</sup></label>
          <input
            type="date" 
            
            className="form-control border border-danger"
            name="date"
            placeholder="yyyy-MM-dd"
            onChange={this.handleChange}
            value={this.state.interview.date}
            required={true}
          />
        </div>
        

        
        <div className="form-group">
          <label htmlFor="recommendation">Recommendation</label>
          <input
            type="recommendation"
            className="form-control"
            name="reccommendation"
            placeholder="remarks"
            onChange={this.handleChange}
            value={this.state.interview.reccommendation}
          />
        </div>
        <div className="form-group ">
          <label htmlFor="panel">Panel</label>
          <select
            className="form-control border border-danger"
            name="panel"
            onClick={this.handleChange}
            required={true}
            >
            <option value="null" disabled>--Select the Panel--</option>
            
            <option value="A">A</option>
            <option value="B" >B</option>
            <option value="C">C</option>
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="ref">References</label>
          <input
            type="text"
            className="form-control"
            name="ref"
            placeholder="references"
            onChange={this.handleChange}
            value={this.state.interview.ref}
          />
        </div>
        
        <div className="buttons form-group">
        
        <input type="reset" className=" interviewclear btn btn-outline-danger " onClick={this.handleClear} value="Reset"/>
        <input type="submit" className="interviewsubmit btn btn-dark " value="Submit" />
        </div>
      </form>
            </div>;
    }
}
export default InterviewForm;