import React from 'react';
import renderTable from './interviewTable';
import axios from 'axios';
import { Link } from 'react-router-dom';
import swal from 'sweetalert';
import { SwipeableDrawer } from '@material-ui/core';

class InterviewList extends React.Component{
    handleDelete=(event)=>{
        event.preventDefault();
        let str=event.target.value;
        //let aadhar=parseInt(str.substring(0,str.indexOf("+")+1));

        //let mode=str.substring(str.indexOf("+")+1);

        //console.log(aadhar,mode);
        let aadhar=str;
        swal({
            title:"Finally,Deleeeeeeeeeete!!!!???",
            text:"you have gone mad ",
            icon:"warning",
            buttons:true
        }).then(del => {
            if(del){
                axios.delete("http://localhost:9080/interview/"+aadhar).then(res=>{
                console.log("delete interview",res);
                if(res.status===200){
                    swal({
                        title:"Success,I told you to delete this man",
                        text:"yeah,my weight got reduced",
                        icon:"success",
                        buttons:true
                    }).then(res=>{
                        window.location.reload();
                    })
                    
                }
            })
            }else{
                swal({
                    title:"Atleast  ,You heard me",
                    text:"no delete,I backed it",
                    icon:"info",
                    
                })
            }
        })
            
           
        
    } 
    
        
     
    render(){
        let data=this.props.data;
        console.log("data",data);
        let intdata=data.map(datum=>{
            let dat=datum;
            let s="/interview/editInterview/"+datum.id;
            let value=datum.id;
            let mydate=datum.date;
            console.log(mydate)
            let source=datum.candidate.gender=="male"?"https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fthumbs.dreamstime.com%2Fb%2Fbusiness-man-profile-icon-african-american-ethnic-male-avatar-hipster-style-fashion-cartoon-guy-beard-portrait-casual-businessman-69046204.jpg&f=1&nofb=1":"https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn.pixabay.com%2Fphoto%2F2016%2F04%2F26%2F07%2F57%2Fwoman-1353825_640.png&f=1&nofb=1";
            return (
                 <div key={datum.id} className="card border-danger mb-3">
        <div className="card-header"><b>Interview Details :</b><h5><i>{datum.candidate.firstName+" "+datum.candidate.lastName}</i></h5></div>
     <img className="card-img-top" 
     src={source} height="300" width="200" alt={datum.candidate.firstName}/>
         < div className="card-body">
            <h5 className="card-title">{"Aadhar: "+datum.candidate.aadharNo}</h5>
            <p className="card-text">{"Skills: "+datum.candidate.primarySkills+" "+datum.candidate.secondarySkills}
            </p>
        <p className="card-text"><b>Mode Of Interview : </b><i>{datum.mode}</i></p>
        <p className="card-text"><b>Date : </b><i>{mydate}</i></p>
        <p className="card-text"><b>Location : </b><i>{datum.location}</i></p>
        <p className="card-text"><b>Recommendation : </b><i>{datum.reccommendation}</i></p>
        <p className="card-text"><b>Reference By : </b><i>{datum.ref}</i></p>
        <p className="card-text"><b>Panel : </b><i>{datum.panel}</i></p>
        <button className="btn btn-outline-danger" value={value} onClick={this.handleDelete}>
            DELETE</button>&nbsp;
            <Link to={s}><button className="btn btn-primary" >EDIT</button></Link>
  
     </div>
     
     </div>
             );
         }
     )
 
 
        return (
        
            <div className="card-columns">
                
            {intdata}
           
  
            
  

  
        </div>);
        
    }
}
export default InterviewList;