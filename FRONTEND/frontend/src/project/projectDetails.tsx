import React from 'react';
import ProjectType from './projectType';
import projects from './projects';

interface IState {
  project: any | null;
}
class ProjectDetails extends React.Component<any, IState> {
  state: IState = {
    project: null
  };
  componentDidMount() {
    const projectId = this.props.match.params.projectId;
    const project = projects.find(project => project.id === projectId);
    this.setState({
      project
    });
  }
  render() {
    let project = this.state.project;
    if (project) {
      return (
        <>
          <div className="p-3 border border-blue">
            <div className="row">
              <div className="col-lg-6 p-3">id : {project.id}</div>
              <div className="col-lg-6 p-3">name : {project.name}</div>{' '}
            </div>
            <div className="row">
              <div className="col-lg-6 p-3">
                description : {project.description}
              </div>
              <div className="col-lg-6 p-3">
                project type:{' '}
                <ProjectType
                  width={50}
                  height={50}
                  type={project.type}
                  onProjectTypeClicked={() => {}}
                />
              </div>
            </div>
          </div>
        </>
      );
    }
    return null;
  }
}

export default ProjectDetails;
