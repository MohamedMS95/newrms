import React from 'react';

class RenderCounter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0
    };
  }

  updateCount = () => {
    this.setState(state => {
      return {
        count: state.count + 1
      };
    });
  };
  render() {
    return <div>{this.props.render(this.state.count, this.updateCount)}</div>;
  }
}

export default RenderCounter;
