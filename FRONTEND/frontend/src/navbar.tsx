import React from 'react';
import { Link } from 'react-router-dom';

const NavBar = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <Link to="/project" className="navbar-brand">
        Projects
      </Link>
      <Link to="/user" className="navbar-brand">
        Users
      </Link>
      <Link to="/home" className="navbar-brand">
        Home
      </Link>
      <Link to="/candidate" className="navbar-brand">
        Candidate
      </Link>
      <Link to="/interview" className="navbar-brand">
        Interview
        </Link>
    </nav>
  );
};

export default NavBar;
