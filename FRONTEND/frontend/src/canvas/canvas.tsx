import React from 'react';
import Circle from './circle';
import Point from './point';

class CircleCanvas extends React.Component {
  circle: Circle = new Circle(0, new Point(0, 0));
  componentDidMount() {
    const canvas: any = document.getElementById('canvas');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    canvas.style.backgroundColor = 'black';
    const context: CanvasRenderingContext2D = canvas.getContext('2d');
    this.context = context;
    this.circle = new Circle(40, new Point(200, 200));
    this.renderCircle(this.circle);

    setInterval(() => {
      this.context.clearRect(0, 0, window.innerWidth, window.innerHeight);
      this.circle
        .getCenterPoint()
        .setX(this.circle.getCenterPoint().getX() + 5);
      this.circle
        .getCenterPoint()
        .setY(this.circle.getCenterPoint().getY() + 5);

      this.renderCircle(this.circle.getCircle());
    }, 100);
  }
  renderCircle(circle: Circle) {
    this.context.save();
    this.context.beginPath();
    this.context.arc(
      circle.getCenterPoint().getX(),
      circle.getCenterPoint().getY(),
      circle.getRadius(),
      0,
      2 * Math.PI
    );
    this.context.fillStyle = 'red';
    this.context.fill();
    this.context.restore();
  }
  render() {
    return <canvas id="canvas"></canvas>;
  }
}

export default CircleCanvas;
