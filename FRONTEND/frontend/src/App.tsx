import React from 'react';
import './App.css';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import ProjectTable from './project/projectTable';
import NavBar from './navbar';
import ProjectDetails from './project/projectDetails';
import store from './store';
import { Provider } from 'react-redux';
import ProjectForm from './project/projectForm';
import Candidate from './project/candidate/candidate';
import CandidateEdit from './project/candidate/candidateEdit';
import ComponentForm from './project/candidate/candidateForm.jsx';
import Interview from './project/interview/interview';
import InterviewForm from './project/interview/interviewForm.jsx';
import Draw from './project/matNavbar';
import InterviewEdit from './project/interview/interviewEdit';
import CandidateFile from './project/candidate/candidateFile';
import Search from './project/candidate/search';
function User() {
  return <div> <h1>User</h1></div>;
}
function Home() {
  return <div className="homepage">
    
    { <img 
    src="https://www.digitallschool.com/wp-content/uploads/2017/09/SmallLogo.png" 
    alt="Home Page"
    height="200"
    width="250"
    /> }
    
    </div>;
}

const App: React.FC = () => {
  return (
    <>
      <Provider store={store}>
        <BrowserRouter>
          <Draw />
          <Switch>
            <Redirect exact={true} from="/" to="/home" />
            <Route exact path="/project" component={ProjectTable} />
            <Route exact path="/project/add" component={ProjectForm} />
            <Route
              exact
              path="/project/:projectId"
              component={ProjectDetails}
            />
            <Route exact path="/user" component={User} />
            <Route exact path="/home" component={Home} />
            <Route exact path="/candidate" component={Candidate}/>
            <Route exact path="/candidate/candidateForm" component={ComponentForm}/>
            <Route name="CandidateEdit" exact path="/candidate/edit/:aadhar" component={CandidateEdit}/>
            <Route exact path="/interview" component={Interview}/>
            <Route exact path="/interview/addInterview" component={InterviewForm}/>
            <Route exact path="/interview/editInterview/:aadhar" component={InterviewEdit}/>
            <Route exact path="/interview/addInterview/:aadhar" component={InterviewForm}/>
            <Route exact path="/candidate/upload" component={CandidateFile}/>
            <Route exact path="/candidate/search" component={Search}/>
            
          </Switch>
        </BrowserRouter>
      </Provider>
    </>
  );
};

export default App;
