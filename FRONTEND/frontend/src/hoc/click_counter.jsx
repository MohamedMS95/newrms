import React from 'react';

class ClickCounter extends React.Component {
  render() {
    return (
      <button className="btn btn-primary" onClick={this.props.updateCount}>
        The Click Count is {this.props.count} {this.props.name}
      </button>
    );
  }
}

export default ClickCounter;
