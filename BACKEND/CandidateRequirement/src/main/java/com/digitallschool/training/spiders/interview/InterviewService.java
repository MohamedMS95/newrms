/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.interview;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author admin
 */
@Service
public class InterviewService {
    
    @Autowired InterviewRepository interviewRepository;
    
    public boolean updateInterviewForACandidate(long aadhar,Interview interview){
        return interviewRepository.updateInterviewForACandidate(aadhar, interview);
    }
    
    public List<Interview> allInterviews(){
        return interviewRepository.allInterviews();
    }
    
    public Interview getInterviewForACandidate(long aadhar){
        return interviewRepository.getInterviewForACandidate(aadhar);
    }
    
    public boolean addInterviewForACandidate(long aadhar,Interview interview){
        return interviewRepository.addInterviewForACandidate(aadhar, interview);
    }
    public boolean deleteInterviewForACandidate(long aadhar){
        return interviewRepository.deleteInterviewForACandidate(aadhar);
    }
    
}
