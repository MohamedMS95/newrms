/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.candidate;

import com.digitallschool.training.spiders.interview.InterviewService;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.eq;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author admin
 */
@Repository
public class CandidateRepository {
    
   @Autowired MongoDbFactory mf;
   @Autowired InterviewService interviewService;
   
   final String DATABASE="mohamed";
   final String CANDIDATE="candidate";
   
   public Candidate getCandidateForInterview(long aadhar){
       Candidate temp=null;
       
        //Consumer<Candidate> candid=e->e;
        
       CodecRegistry codec=CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        
       MongoCollection collection=mf.getDb(DATABASE).getCollection(CANDIDATE, Candidate.class)
		.withCodecRegistry(codec);
       
        FindIterable<Candidate> iter=collection.find(eq("aadharNo",aadhar),Candidate.class);
        for(Candidate c:iter){
            temp=c;
        }
       return temp;
   }
   
   public boolean updateCandidate(long aadharNo, Candidate candidate) {
        try {
            CodecRegistry codec = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                    CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
            
          MongoCollection collection=  mf.getDb(DATABASE).getCollection(CANDIDATE, Candidate.class).withCodecRegistry(codec);
           collection.replaceOne(eq("aadharNo", aadharNo), candidate);
           return true;
        } catch (MongoException e) {
            e.printStackTrace();
        }
        return false;
    }
   public boolean deleteCandidate(long aadharNo){
       CodecRegistry codec=CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        
       MongoCollection collection=mf.getDb(DATABASE).getCollection(CANDIDATE, Candidate.class)
		.withCodecRegistry(codec);
       
                collection.deleteOne(eq("aadharNo",aadharNo));
                interviewService.deleteInterviewForACandidate(aadharNo);
       return true;
   }
   public boolean addCandidate(Candidate candidate){
       CodecRegistry codec=CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        
       MongoCollection collection=mf.getDb(DATABASE).getCollection(CANDIDATE, Candidate.class)
		.withCodecRegistry(codec);
       
       collection.insertOne(candidate);
       collection.createIndex(eq("aadharNo",candidate.getAadharNo()));
        
       return true;
   }
   public List<Candidate> allCandidates(){
       List<Candidate> candidates=new ArrayList();
       
        CodecRegistry codec=CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        
        Consumer<Candidate> candid=e->candidates.add(e);
        
       mf.getDb(DATABASE).getCollection(CANDIDATE, Candidate.class)
		.withCodecRegistry(codec)
		.find().forEach(candid);
       
        return candidates;
   }
}
