/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.interview;

import com.digitallschool.training.spiders.candidate.Candidate;
import com.digitallschool.training.spiders.candidate.CandidateService;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author admin
 */
@Repository
public class InterviewRepository {
    @Autowired DataSource ds;
    
    @Autowired CandidateService candidateService;
    
    public boolean updateInterviewForACandidate( long aadhar, Interview interview){
        try(Connection con=ds.getConnection();
                    PreparedStatement pst=con.prepareStatement("UPDATE interview SET mode=?,day=?,location=?,recommendation=?,"
                            + "panel=?,ref=? WHERE candidateaadhar=?");
                    ){
                pst.setString(1,interview.getMode());
                
                pst.setDate(2,Date.valueOf(interview.getDate()));
                pst.setString(3,interview.getLocation());
                pst.setString(4,interview.getReccommendation());
                pst.setString(5,interview.getPanel());
                pst.setString(6,interview.getRef());
                pst.setLong(7,aadhar);
                if(pst.executeUpdate()!=0){
                    return true;
                }
                pst.close();
                con.close();
            }catch(SQLException e){
                e.printStackTrace();
            }
        return false;
    }
    public List<Interview> allInterviews(){
        List<Interview> list=new ArrayList();
        Candidate candidate=null;
        try(Connection con=ds.getConnection();
            PreparedStatement pst=con.prepareStatement("SELECT * from interview ");
            ResultSet rs=pst.executeQuery()){
                while(rs.next()){
                    candidate=candidateService.findCandidate(rs.getLong(2));
                Date date=rs.getDate(4);
                SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd");
                String str=formatter.format(date);
                list.add(new Interview(candidate,rs.getString(3),str,rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8)));
            
                    
                }
                rs.close();
                pst.close();
                con.close();
            return list;
            }catch(SQLException e){
                e.printStackTrace();
            }
        return list;
    }
    public boolean deleteInterviewForACandidate(long aadhar){
        try(Connection con=ds.getConnection();
                    PreparedStatement pst=con.prepareStatement("DELETE FROM interview WHERE candidateaadhar=?");
                    ){
                pst.setLong(1, aadhar);
                if(pst.executeUpdate()!=0){
                    return true;
                }
                pst.close();
                con.close();
            }catch(SQLException e){
                e.printStackTrace();
            }
        return false;
    }
    public boolean addInterviewForACandidate(long aadhar,Interview interview){
        
        Candidate candidate;
        candidate=candidateService.findCandidate(aadhar);
        System.out.println("===========is out==========");
            
        if(candidate!=null){
            System.out.println("===========is in==========");
            try(Connection con=ds.getConnection();
                    PreparedStatement pst=con.prepareStatement("INSERT INTO interview VALUES(0,?,?,?,?,?,?,?)");
                    ){
                pst.setLong(1,aadhar);
                pst.setString(2,interview.getMode());
                
                pst.setDate(3,Date.valueOf(interview.getDate()));
                pst.setString(4,interview.getLocation());
                pst.setString(5,interview.getReccommendation());
                pst.setString(6,interview.getPanel());
                pst.setString(7,interview.getRef());
                if(pst.executeUpdate()!=0){
                    return true;
                }
                pst.close();
                con.close();
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
        return false;
    }
    
    public Interview getInterviewForACandidate(long aadhar){
        Interview interview=null;
        Candidate candidate;
        candidate=candidateService.findCandidate(aadhar);
        if(candidate!=null){
            try(Connection con=ds.getConnection();
                    PreparedStatement pst=con.prepareStatement("SELECT * from interview where candidateaadhar=?");
                    ){
                pst.setLong(1,aadhar);
            ResultSet rs=pst.executeQuery();
            if(rs.next()){
                Date date=rs.getDate(4);
                SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd");
                String str=formatter.format(date);
                interview=new Interview(candidate,rs.getString(3),str,rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8));
            }
            rs.close();
            return interview;
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
        return interview;
    }
    
}
