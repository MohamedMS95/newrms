/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.interview;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author admin
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/interview")
public class InterviewController {
    
    @Autowired InterviewService interviewService;
    
    @GetMapping
    public List<Interview> allInterviews(){
        return interviewService.allInterviews();
    }
    
    @GetMapping("/{aadhar}")
    public Interview getInterviewForACandidate(@PathVariable("aadhar") long aadhar){
        Interview inter=interviewService.getInterviewForACandidate(aadhar);
         return inter;
    }
    @PutMapping
    public ResponseEntity<String> updateInterviewForACandidate(@RequestBody Interview interview){
        long aadhar=interview.getCandidate().getAadharNo();
        if(interviewService.updateInterviewForACandidate(aadhar, interview)){
            return new ResponseEntity<>("updated successfully",HttpStatus.OK);
        }
        return new ResponseEntity<>("not updated successfully",HttpStatus.EXPECTATION_FAILED);
    }
    @PostMapping
    public ResponseEntity<String> addInterview(@RequestBody Interview interview){
        long aadhar=interview.getCandidate().getAadharNo();
        if(interviewService.addInterviewForACandidate(aadhar, interview)){
            return new ResponseEntity<>("redirect:/interview",HttpStatus.OK);
        
        }else{
            return new ResponseEntity<>("Interrview is not created",HttpStatus.EXPECTATION_FAILED);
        }
        
    }
    @DeleteMapping("/{aadhar}")
    public String  deleteInterviewForACandidate(@PathVariable long aadhar){
        if(interviewService.deleteInterviewForACandidate(aadhar)){
            return "redirect:/interview";
        }else{
            return "Not deleted";
        }
    }
    
    
    @ModelAttribute("inter")
    public Interview returnInterview(){
        return new Interview();
    }
    
    
}
