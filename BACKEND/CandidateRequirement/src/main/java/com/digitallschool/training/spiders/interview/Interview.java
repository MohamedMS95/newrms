/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.interview;

import com.digitallschool.training.spiders.candidate.Candidate;
import java.time.LocalDate;

/**
 *
 * @author admin
 */
public class Interview {
    private Candidate candidate;
    private String mode;
    private String date;
    private String location;
    private String reccommendation;
    private String  panel;
    private String ref;
    
    public Interview(){
        super();
    }

    public Interview(Candidate candidate, String mode, String date, String location, String reccommendation, String panel, String ref) {
        this.candidate = candidate;
        this.mode = mode;
        this.date = date;
        this.location = location;
        this.reccommendation = reccommendation;
        this.panel = panel;
        this.ref = ref;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getReccommendation() {
        return reccommendation;
    }

    public void setReccommendation(String reccommendation) {
        this.reccommendation = reccommendation;
    }

    public String getPanel() {
        return panel;
    }

    public void setPanel(String panel) {
        this.panel = panel;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }
    
    
}
