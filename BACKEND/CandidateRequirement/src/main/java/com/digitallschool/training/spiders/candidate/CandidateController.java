/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.candidate;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author admin
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/candidate")
public class CandidateController {
    @Autowired
    CandidateService candidateService;
    @GetMapping("/each/{aadhar}")
    public Candidate getCandidate(@PathVariable long aadhar){
        return candidateService.findCandidate(aadhar);
    }
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Candidate> viewCandidate(){
        return candidateService.getCandidates();
   }
    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity addCandidate(@Valid @RequestBody  Candidate candidate){
        System.out.println("==========================="+candidate.getAadharNo()+"===============================");
        if(candidateService.addCandidate(candidate)){
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
    }
    @DeleteMapping("{aadhar}")
    public ResponseEntity<?> deleteCandidate(@PathVariable("aadhar") long aadhar){
        System.out.println(aadhar);
        if(candidateService.deleteCandidate(aadhar)){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }
    @PutMapping
    public ResponseEntity<String> updateCandidate(@RequestBody Candidate candidate){
        long aadhar=candidate.getAadharNo();
        System.out.println(aadhar);
        if(candidateService.updateCandidate(aadhar, candidate)){
        return new ResponseEntity("candidate is updated sucessfully",HttpStatus.OK);
        }else{
        return new ResponseEntity<>("Candidate is not updated",HttpStatus.EXPECTATION_FAILED);
        }
    }
    
}
