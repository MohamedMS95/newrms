/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.candidate;
import java.time.LocalDate;
import javax.validation.constraints.Size;
import org.bson.codecs.pojo.annotations.BsonProperty;

/**
 *
 * @author admin
 */
public class Candidate {
    @BsonProperty("aadharNo")
    private long aadharNo;
    @Size(max = 4)
    private String firstName;
    private String lastName;
    private String gender;
    private String date;
    private long mobile;
    private long altMobile;
    private double totalexp;
    private double relexp;
    private String email;
    private String primarySkills;
    private String secondarySkills;
    private String qualification;
    private String addCertification;
    private double expectedCTC;
    private double currentCTC;
    private String currentAddress;
    private String altAddress;

    public double getTotalexp() {
        return totalexp;
    }

    public void setTotalexp(double totalexp) {
        this.totalexp = totalexp;
    }

    public double getRelexp() {
        return relexp;
    }

    public void setRelexp(double relexp) {
        this.relexp = relexp;
    }

    public long getAadharNo() {
        return aadharNo;
    }

    public void setAadharNo(long aadharNo) {
        this.aadharNo = aadharNo;
    }

    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getMobile() {
        return mobile;
    }

    public void setMobile(long mobile) {
        this.mobile = mobile;
    }

    public long getAltMobile() {
        return altMobile;
    }

    public void setAltMobile(long altMobile) {
        this.altMobile = altMobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPrimarySkills() {
        return primarySkills;
    }

    public void setPrimarySkills(String primarySkills) {
        this.primarySkills = primarySkills;
    }

    public String getSecondarySkills() {
        return secondarySkills;
    }

    public void setSecondarySkills(String secondarySkills) {
        this.secondarySkills = secondarySkills;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getAddCertification() {
        return addCertification;
    }

    public void setAddCertification(String addCertification) {
        this.addCertification = addCertification;
    }

    public double getExpectedCTC() {
        return expectedCTC;
    }

    public void setExpectedCTC(double expectedCTC) {
        this.expectedCTC = expectedCTC;
    }

    public double getCurrentCTC() {
        return currentCTC;
    }

    public void setCurrentCTC(double currentCTC) {
        this.currentCTC = currentCTC;
    }

    public String getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }

    public String getAltAddress() {
        return altAddress;
    }

    public void setAltAddress(String altAddress) {
        this.altAddress = altAddress;
    }
    @Override
    public String toString(){
        return "["+this.aadharNo+","+this.firstName+","+this.lastName+","+this.date+","+this.email
                + this.gender+"]";
    }
    
}
